using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zad2 : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    Vector3 nextpos;
    Vector3 prevpos;
    bool direction = true;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        prevpos = transform.position;
        nextpos = new Vector3(transform.position.x + 10 , transform.position.y, transform.position.z);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        float mH = Input.GetAxis("Horizontal");
        if(Input.GetAxis("Horizontal") > 0)
        {
            Vector3 velocity;
            velocity = nextpos.normalized * speed * Time.deltaTime;
            if (direction)
            {
                rb.MovePosition(transform.position + velocity);
                if (transform.position.x >= nextpos.x)
                    direction = false;
            }
            // wykonujemy przesunięcie Rigidbody z uwzględnieniem sił fizycznych
           
            if (!direction)
            {
                rb.MovePosition(transform.position - velocity );
                if (transform.position.x <= prevpos.x)
                    direction = true;
            }
        }
    }
}
