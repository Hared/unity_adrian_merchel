using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zad3 : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    Vector3 nextposx;
    Vector3 nextposz;
    Vector3 prevpos;
    int direction = 0;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        prevpos = transform.position;
        nextposx = new Vector3(transform.position.x + 10, transform.position.y, transform.position.z);
        nextposz = new Vector3(transform.position.x , transform.position.y, transform.position.z + 10);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float mH = Input.GetAxis("Horizontal");
        if (Input.GetAxis("Horizontal") > 0)
        {
            
           
            if (direction == 0)
            {
                Vector3 velocity;
                velocity = nextposx.normalized * speed * Time.deltaTime;
                rb.MovePosition(transform.position + velocity);
                if (transform.position.x >= nextposx.x)
                {
                    transform.Rotate(0, -90, 0, Space.Self);
                    direction = 1;
                }
            }
            if (direction == 1)
            {
                Vector3 velocity;
                velocity = nextposz.normalized * speed * Time.deltaTime;
                rb.MovePosition(transform.position + velocity);
                if (transform.position.z >= nextposz.z)
                {
                    transform.Rotate(0, 90, 0, Space.Self);
                    direction = 2;
                }
            }
            if (direction == 2)
            {
                Vector3 velocity;
                velocity = nextposx.normalized * speed * Time.deltaTime;
                rb.MovePosition(transform.position - velocity);
                if (transform.position.x <= prevpos.x)
                {
                    transform.Rotate(0, -90, 0, Space.Self);
                    direction = 3;
                }
            }
            if (direction == 3)
            {
                Vector3 velocity;
                velocity = nextposz.normalized * speed * Time.deltaTime;
                rb.MovePosition(transform.position - velocity);
                if (transform.position.z <= prevpos.z)
                {
                    transform.Rotate(0, 90, 0, Space.Self);
                    direction = 0;
                }
            }
        }
    }
}
