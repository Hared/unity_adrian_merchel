using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zad1lab5 : MonoBehaviour
{
    public float elevatorSpeed = 2f;
    public bool isRunning = false;
    public bool isReturn = false;
    public float distance = 6.6f;
    public List<Vector3> waypoints;
    public int current;
    public int waypointsc;
    public Vector3 start;
    // Start is called before the first frame update
    void Start()
    {
        current = 0;
        start = transform.position;
        Debug.Log(waypoints.Count);
    }

    // Update is called once per frame
    void Update()
    {


        if (isRunning)
        {
            if (!isReturn)
            {
                if (Vector3.Distance(transform.position, waypoints[current]) < 1f)
                {
                    if (current < waypoints.Count - 1)
                        current++;
                    if (current == waypoints.Count - 1)
                        isReturn = true;
                }

                
                    transform.position = Vector3.MoveTowards(transform.position, waypoints[current], elevatorSpeed * Time.deltaTime);
                


            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, start, elevatorSpeed * Time.deltaTime);
            }


        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isReturn = false;
            isRunning = true;
            Debug.Log("Gracz wszed�");
         
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isReturn = true;
            current = 0;
            Debug.Log("Gracz Wyszed�");
        }
    }
}

