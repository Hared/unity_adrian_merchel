using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class RandomCubesGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    List<Vector3> positions = new List<Vector3>();
    public float delay = 3.0f;
    int objectCounter = 0;
    public int objects;
    public Collider platform;
    public List<Material> materialy;
    // obiekt do generowania
    public GameObject block;

    void Start()
    {

        // w momecie uruchomienia generuje 10 kostek w losowych miejscach
        List<int> pozycje_x = new List<int>(Enumerable.Range(-(int)platform.bounds.size.x / 2, (int)platform.bounds.size.x/2).OrderBy(x => Guid.NewGuid()).Take(objects));
        List<int> pozycje_z = new List<int>(Enumerable.Range(-(int)platform.bounds.size.z / 2, (int)platform.bounds.size.z/2).OrderBy(x => Guid.NewGuid()).Take(objects));

        for (int i = 0; i < objects; i++)
        {
            Debug.Log(i);
            this.positions.Add(new Vector3(pozycje_x[i], 5, pozycje_z[i]));
           
        }
        foreach (Vector3 elem in positions)
        {
            Debug.Log(elem);
        }
        // uruchamiamy coroutine
        StartCoroutine(GenerujObiekt());
    }

    void Update()
    {

    }

    IEnumerator GenerujObiekt()
    {
        Debug.Log("wywo�ano coroutine");
        foreach (Vector3 pos in positions)
        {
            
            GameObject tmpobject = Instantiate(this.block, this.positions.ElementAt(this.objectCounter++), Quaternion.identity);
            tmpobject.GetComponent<MeshRenderer>().material = materialy[UnityEngine.Random.Range(0, materialy.Count )];
            yield return new WaitForSeconds(this.delay);
        }
        // zatrzymujemy coroutine
        StopCoroutine(GenerujObiekt());
    }
}
